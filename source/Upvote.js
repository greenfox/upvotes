let THREE = require("three");
let CANNON = require("cannon");

/*
   /\
  /  \      tw, th for width and height of top
 /    \
/      \ ______ this line is center. will I have to move this for physics? (do I care?)
   ||       bw, bh for width and height of bot
   ||
   ||
plus z depth
*/

module.exports = class Upvote {
    constructor(args) {
        this.args = args
        let geo = this.UpvoteThreeGeo({
            tw: 1.5,
            bh: 1, bw: 0.5,
            z: 0.25
        })

        let material = new THREE.MeshBasicMaterial({ color: 0x00ff00});
        this.three = new THREE.Mesh(geo, material)
        this.cannon = this.upvoteCannonGeo()
        this.setPosition()

        delete this.args
    }
    Update() {
        if(this.cannon.position.y < -25){
            this.setPosition();
        }
        this.three.position.copy(this.cannon.position)
        this.three.quaternion.copy(this.cannon.quaternion)

    }
    setPosition(){
        let n = 50
        let h = 100
        this.cannon.position.y = h + ((Math.random() * n)-(n/2))
        this.cannon.position.x = ((Math.random() * n)-(n/2))
        this.cannon.position.z = 0
        this.cannon.velocity.copy(new CANNON.Vec3(0,0,0))

    }
    upvoteCannonGeo() {
        let a = new THREE.Box3().setFromObject(this.three);
        let x = (a.max.x - a.min.x)
        let box = new CANNON.Box(//ugly
            new CANNON.Vec3(
                (a.max.x - a.min.x),
                (a.max.y - a.min.y),
                (a.max.z - a.min.z)
            )
        )
        let phyBody = new CANNON.Body({ mass: 1, shape: box }); //todo better shape
        return phyBody
        //for now,we'll just treat these as cubes?
    }
    UpvoteThreeGeo(args) {
        let top = new THREE.CylinderGeometry(args.tw / 2, args.tw / 2, args.z, 3);
        top.rotateX(-Math.PI / 2)
        top.translate(0, 0, 0);

        let bot = new THREE.BoxGeometry(args.bw, args.bh, args.z);
        bot.translate(0, -args.bh / 2, 0);
        var temp = bot.merge(top)
        return bot;
    }
}

//good enough!




// var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
// var cube = new THREE.Mesh( geometry, material );
