let THREE = require("three");
let CANNON = require("cannon");

let Upvote = require("./Upvote")

//threejs settup
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

var renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

//cannonjs settup
var world = new CANNON.World();
world.gravity.set(0,-9.82, 0);
world.broadphase = new CANNON.NaiveBroadphase();

//simple for now
// var groundShape = new CANNON.Plane();
// var groundBody = new CANNON.Body({ mass: 0, shape: groundShape });
// groundBody.position.y = -10
// // groundBody.quaternion.setFromEuler(Math.PI/2,0,0)
// world.add(groundBody);

let upvotes =[]
for( let i = 0; i < 1000; i++){
    upvotes[i] = new Upvote()
    scene.add(upvotes[i].three);
    world.add(upvotes[i].cannon);
        
}


camera.position.z = 20;
camera.rotateX(Math.PI * 0.2)
var animate = function () {
    requestAnimationFrame(animate);

    world.step(1 / 60);//todo, timestep by framerate
    
    upvotes.forEach((myUpvote)=>{
        myUpvote.Update()
    })
    // console.log(myUpvote.cannon.position)
    renderer.render(scene, camera);
};

animate();
